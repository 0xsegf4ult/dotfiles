local awful = require("awful");
local gears = require("gears");
local beautiful = require("beautiful");
--local icons = require("theme.icons");

awful.layout.layouts = {
    awful.layout.suit.tile,
    awful.layout.suit.tile.left,
    awful.layout.suit.tile.bottom,
    awful.layout.suit.fair,
    awful.layout.suit.fair.horizontal,
    awful.layout.suit.spiral
};

local tags = {
	{
		icon = beautiful.topbar.terminal_normal,
		name = "terminal",
		screen = 1,
	},
	{
		icon = beautiful.topbar.code_normal,
		name = "dev",
		screen = 1,
	},
	{
		icon = beautiful.topbar.browser_normal,
		name = "browser",
		screen = 1,
	},
	{
		icon = beautiful.topbar.social_normal,
		name = "social",
		screen = 1,
	},
	{
		icon = beautiful.topbar.generic_normal,
		name = "any",
		screen = 1,
	}
};

awful.screen.connect_for_each_screen(
	function(s)
		for i, tag in pairs(tags) do
			awful.tag.add(
				i,
				{
					icon = tag.icon,
					name = tag.name,
					layout = awful.layout.suit.tile,
					gap_single_client = true,
					gap = 5,
					screen = s,
					selected = i == 1
				}
			)
		end
	end
);

