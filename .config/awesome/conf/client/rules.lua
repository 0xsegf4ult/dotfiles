local awful = require("awful");
local gears = require("gears");
local client_keys = require("conf.client.keys");
local client_buttons = require("conf.client.buttons");
local beautiful = require("beautiful");

awful.rules.rules = {
	{ 
		rule = {},
		properties = { 
		    border_width = beautiful.border_width,
			border_color = beautiful.border_normal,
			focus = awful.client.focus.filter,
			raise = true,
			keys = client_keys,
			buttons = client_buttons,
			screen = awful.screen.preferred,
			placement = awful.placement.no_overlap+awful.placement.no_offscreen
		}
	},

	-- Floating clients.
	{
		rule_any = {
			instance = {
				"DTA",
				"copyq",
				"pinentry",
			},
			class = {
				"Arandr",
				"Gpick",
				"Kruler",
				"MessageWin",
				"Sxiv",
				"Tor Browser",
				"veromix",
				"xtightvnxviewer"
			},

			name = {
				"Event Tester",
			},
			role = {
				"AlarmWindow",
				"ConfigManager",
				"pop-up",
			}
		},
		properties = { floating = true }
	},
	{
		rule = { class = "osu!.exe" },
		properties = { floating = true, fullscreen = true}
	},
	{
		rule = { class = "steam_app_1172470" },
		properties = { floating = true, fullscreen = true}
	},
	{
		rule = { class = "GLava" },
		properties = { 
			floating = true, 
			fullscreen = true, 
			border_width = 0,
			below = true,
			focusable = false,
			sticky = true,
		}
	},
	-- Titlebars
	{ 
		rule_any = {
			type = { "normal", "dialog" }
		},
		properties = { titlebars_enabled = true }
	}
};

