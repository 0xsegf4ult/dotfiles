local awful = require("awful")
local wibox = require("wibox")
local naughty = require("naughty")
local gears = require("gears")
local beautiful = require("beautiful")

local home = os.getenv("HOME")

local system_widgets = {
    {
        name = "notifications",
        normal_image = beautiful.topbar.bell_normal,
        hover_image = beautiful.topbar.bell_hover,
        -- margins = {
            top = 6,
            bottom = 6,
        -- },
        command = function(system_widget)
			if(naughty.is_suspended()) then
				naughty.resume();
				system_widget.norm_i = beautiful.topbar.bell_normal;
			else 
				naughty.suspend();
				system_widget.norm_i = beautiful.topbar.bell_off;
			end;
		end,
    },
};

local function create_buttons(s)

    local all_buttons = {};

    for _, tab in pairs(system_widgets) do

        local normal_image = tab.normal_image;
        local hover_image = tab.hover_image or normal_image;

        local system_widget = wibox.widget({
            screen = s,
			norm_i = normal_image,
            hv_i = hover_image,
			widget = wibox.container.background,
            bg = "#ff800000",
            {
                widget = wibox.container.place,
                {
                    widget = wibox.container.margin,
                    top = tab.top,
                    right = tab.right,
                    bottom = tab.bottom,
                    left = tab.left,
                    {
                        id = "button_img",
                        widget = wibox.widget.imagebox,
                        image = tab.normal_image,
                    },
                },
            }
        });

        local button_image = system_widget:get_children_by_id("button_img")[1];

        system_widget:connect_signal("mouse::enter", function()
            button_image.image = system_widget.hv_i;
        end );

        system_widget:connect_signal("mouse::leave", function()
            button_image.image = system_widget.norm_i;
        end );

        system_widget:connect_signal("button::press", function()
            button_image.image = system_widget.norm_i;
        end );

        system_widget:connect_signal("button::release", function()
            button_image.image = system_widget.hv_i;
            tab.command(system_widget);
        end);

        table.insert(all_buttons, #all_buttons + 1, system_widget);
    end;

    return all_buttons;
end;

local function make_system_buttons(s)
		
	local layout_box = wibox.widget({
		widget = wibox.container.margin,
		top = 6,
		bottom = 6,
		{
			layout = wibox.layout.fixed.horizontal,
			awful.widget.layoutbox(s),
		}
	});

	layout_box:buttons(gears.table.join(
		awful.button({}, 1, function() awful.layout.inc(1) end),
		awful.button({}, 3, function() awful.layout.inc(-1) end),
		awful.button({}, 4, function() awful.layout.inc(1) end),
		awful.button({}, 5, function() awful.layout.inc(-1) end)
	));

	local final_buttons = ({
		spacing = 15,
		widget = wibox.container.background,
		layout = wibox.layout.fixed.horizontal,
	});

	for _, v in pairs( create_buttons(s, system_widgets) ) do
		table.insert(final_buttons, #final_buttons + 1, v);
	end;

	final_buttons[#final_buttons + 1] = layout_box;

	local clock = wibox.widget.textclock("%H:%M");

	final_buttons[#final_buttons + 1] = clock;


	return wibox.widget(final_buttons);
end;

local function make_widget(s)
	local setup_widget = wibox.widget({
		layout = wibox.layout.fixed.horizontal,
		make_system_buttons(s)
	});

	return setup_widget;
end;

return make_widget;
