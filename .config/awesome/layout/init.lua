local awful = require("awful");
local wibox = require("wibox");
local beautiful = require("beautiful");

local make_system_widgets = require("layout.system_widgets")
local make_tags = require("layout.tag_list");

awful.screen.connect_for_each_screen(function(scr)
	scr.topbar = awful.wibar({
		position = "top", 
		height = 28,
		screen = scr, 
		type = "dock",
		bg = beautiful.bg_normal .. "00", 
		fg = beautiful.fg,
	});

	local bar_layout = {
	layout = wibox.layout.manual,
	{ -- Left side
		point = { x = 0, y = 0 },
		forced_width = awful.screen.focused().geometry.width / 2 - 60,
		forced_height = 28,
		widget = wibox.container.background,
		{
			widget = wibox.container.margin,
			left = 15,
			{
				widget = wibox.container.place,
				halign = "left",
				{
					layout = wibox.layout.fixed.horizontal,
					make_tags(scr)
				}
			}
		}
	},
	{ -- Right side
        point = { x = awful.screen.focused().geometry.width / 2 + 60, y = 0 },
        forced_width = awful.screen.focused().geometry.width / 2 - 60,
        forced_height = 28,
        widget = wibox.container.background,
        {
            widget = wibox.container.margin,
            right = 15,
            {
                widget = wibox.container.place,
                halign = "right",
                {
                    layout = wibox.layout.fixed.horizontal,
                    make_system_widgets(scr),
                }
            },
        },  
    }
};


	scr.topbar:setup(bar_layout);
end );


