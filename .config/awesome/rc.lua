local gears = require("gears");
local awful = require("awful");

local beautiful = require("beautiful");
beautiful.init(os.getenv("HOME") .. "/.config/awesome/theme/theme.lua");

require("layout");

require("modules.startup");
require("modules.notifications");

require("conf.client");
require("conf.tags");
_G.root.keys(require("conf.keys.global"));

awful.screen.connect_for_each_screen(
  function(s)
    -- If wallpaper is a function, call it with the screen
    if beautiful.wallpaper then
        if type(beautiful.wallpaper) == "string" then
            if beautiful.wallpaper:sub(1, #"#") == "#" then
                gears.wallpaper.set(beautiful.wallpaper)
            elseif beautiful.wallpaper:sub(1, #"/") == "/" then
                gears.wallpaper.maximized(beautiful.wallpaper, s)
            end
        else
            beautiful.wallpaper(s)
        end
    end
  end
)

_G.client.connect_signal("manage", function(c)
	if not _G.awesome.startup then
		awful.client.setslave(c);
	else
		if not c.size_hints.user_position
		and not c.size_hints.program_position then
			awful.placement.no_offscreen(c);
		end;
	end
end);

client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end);
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end);

