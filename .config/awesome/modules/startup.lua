local awful = require("awful");

local autostart = {
	"picom -b --experimental-backends",
	"easyeffects --gapplication-service",
	"nvidia-settings --load-config-only",
	"/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1"
};

for _, v in pairs(autostart) do
	awful.spawn.once(v);
end;


