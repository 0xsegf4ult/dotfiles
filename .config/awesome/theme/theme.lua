local gears = require("gears");
local themes_path = gears.filesystem.get_themes_dir();

local theme = {topbar = {}};

theme.dir					= os.getenv("HOME") .. "/.config/awesome/theme";
theme.wallpaper 			= theme.dir .. "/background.jpg";
theme.border_width			= 1;
theme.border_normal 		= "#000000";
theme.border_focus 			= "#434c5e";
theme.border_marked 		= "#91231c";
theme.font					= "Roboto";
theme.bg_normal                                 = "#4347594a"
theme.fg_normal                                 = "#FFFFFF"
theme.bg_focus                                  = "#4347594a"
theme.fg_focus                                  = "#232323"
theme.bg_urgent                                 = "#C92132"
theme.fg_urgent                                 = "#282828"
theme.fg_widget                                 = "#32302f"
theme.titlebar_bg_normal                        = "#3f3f3f"
theme.titlebar_fg_normal                        = "#282828"

theme.titlebar_bg_focus                         = "#2b5355"
theme.titlebar_fg_focus                         = "#282828"

theme.menu_height                               = 16
theme.menu_width                                = 140

theme.taglist_bg_focus							= "#434759c8"

theme.notification_font                         = "Roboto"
theme.notification_bg                           = theme.bg_normal
theme.notification_fg                           = theme.fg_normal
theme.notification_border_color                 = theme.bg_normal
theme.notification_opacity                      = 1

theme.topbar.networking_normal					= theme.dir .. "/icons/wifi_normal.svg"
theme.topbar.bell_normal						= theme.dir .. "/icons/bell_normal.svg"
theme.topbar.bell_hover							= theme.dir .. "/icons/bell_hover.svg"
theme.topbar.bell_off							= theme.dir .. "/icons/bell_slash_normal.svg"
theme.topbar.power_normal						= theme.dir .. "/icons/power_normal.svg"
theme.topbar.power_hover						= theme.dir .. "/icons/power_hover.svg"
theme.topbar.terminal_normal						= theme.dir .. "/icons/console.svg"
theme.topbar.code_normal						= theme.dir .. "/icons/code-braces.png"
theme.topbar.browser_normal						= theme.dir .. "/icons/google-chrome.svg"
theme.topbar.social_normal						= theme.dir .. "/icons/forum.svg"
theme.topbar.generic_normal						= theme.dir .. "/icons/flask.svg"

theme.layout_fairh = themes_path.."default/layouts/fairhw.png"
theme.layout_fairv = themes_path.."default/layouts/fairvw.png"
theme.layout_floating  = themes_path.."default/layouts/floatingw.png"
theme.layout_magnifier = themes_path.."default/layouts/magnifierw.png"
theme.layout_max = themes_path.."default/layouts/maxw.png"
theme.layout_fullscreen = themes_path.."default/layouts/fullscreenw.png"
theme.layout_tilebottom = themes_path.."default/layouts/tilebottomw.png"
theme.layout_tileleft   = themes_path.."default/layouts/tileleftw.png"
theme.layout_tile = themes_path.."default/layouts/tilew.png"
theme.layout_tiletop = themes_path.."default/layouts/tiletopw.png"
theme.layout_spiral  = themes_path.."default/layouts/spiralw.png"
theme.layout_dwindle = themes_path.."default/layouts/dwindlew.png"
theme.layout_cornernw = themes_path.."default/layouts/cornernww.png"
theme.layout_cornerne = themes_path.."default/layouts/cornernew.png"
theme.layout_cornersw = themes_path.."default/layouts/cornersww.png"
theme.layout_cornerse = themes_path.."default/layouts/cornersew.png"

return theme;
